
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AutomobileList from './AutomobileList';
import ManufacturerList from './ManufacturerList';
import AutoModelList from './ModelList';
import ModelForm from './ModelForm';
import AutomobileForm from './AutomobileForm';
import ManufacturerForm from './ManufacturerForm';
import TechnicianList from './TechnicianList';
import SalespersonList from './SalespersonList';
import CustomerList from './CustomerList';
import SalespersonForm from './SalespersonForm';
import CustomerForm from './CustomerForm';
import AppointmentsList from './AppointmentList';
import AppointmentsForm from './AppointmentForm';
import ServiceHistoryList from './ServiceHistory';
import SaleList from './SaleList';
import SaleForm from './SaleForm';
import TechnicianForm from './TechnicianForm';
import SaleHistory from './SaleHistory';
function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="automobiles">
            <Route path="" element={<AutomobileList automobiles={props.automobiles} />} />
            <Route path="new" element={<AutomobileForm />} />
            </Route>
          <Route path="manufacturers">
            <Route path="" element={<ManufacturerList manufacturers={props.manufacturers} />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
          <Route path="" element={<AutoModelList models={props.models} />} />
          <Route path="new" element={<ModelForm />} />
          </Route>
          <Route path="technicians">
            <Route path="" element={<TechnicianList technicians={props.technicians} />} />
            <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments">
            <Route path="" element={<AppointmentsList appointments={props.appointments} />} />
            <Route path="new" element={<AppointmentsForm />} />
            <Route path="history" element={<ServiceHistoryList />} />
          </Route>
          <Route path="salespeople">
            <Route path="" element={<SalespersonList salespeople={props.salespeople} />} />
            <Route path="new" element={<SalespersonForm />} />
          </Route>
          <Route path="customers">
            <Route path="" element={<CustomerList customers={props.customers} />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="sales">
            <Route path="" element={<SaleList customers={props.sales} />} />
            <Route path="new" element={<SaleForm />} />
            <Route path="history" element={<SaleHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
