
import React, { useEffect, useState } from 'react';

function ManufacturerList() {
    const [ manufacturers, setManufacturers ] = useState([]);

    const fetchData = async () => {
        const manufacturersUrl = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(manufacturersUrl);
        if (response.ok) {
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }
    };
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <main className="container">
            <h1 className="h1-padded">Manufacturers</h1>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturer => {
                        return (
                            <tr key={manufacturer.href}>
                                <td>{manufacturer.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </main>
    )
}
export default ManufacturerList;
