import React, { useState } from "react";

function TechnicianForm() {
  const [formData, setFormData] = useState({
    first_name: "",
    last_name: "",
    employee_id: "",
  });

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const response = await fetch("http://localhost:8080/api/technicians/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    });

    if (response.ok) {
      console.log("Technician created!");
      setFormData({
        first_name: "",
        last_name: "",
        employee_id: "",
      });
    } else {
      console.error("Failed to create technician");
    }
  };

  return (
    <main>
    <div className="row">
    <div className="offset-3 col-6"> 
    <div className="shadow p-4 mt-4">
      <h2>Create Technician</h2>
      <form onSubmit={handleSubmit} className="technician-form">
      <div className="form-floating mb-3">
        <input
          placeholder="First Name"
          type="text"
          id="first_name"
          name="first_name"
          value={formData.first_name}
          onChange={handleChange}
          className="form-control"
        />
        <label htmlFor="first_name">First Name:</label>
      </div>
      <div className="form-floating mb-3">
        <input
          placeholder="Last Name"
          type="text"
          id="last_name"
          name="last_name"
          value={formData.last_name}
          onChange={handleChange}
          className="form-control"
        />
        <label htmlFor="last_name">Last Name:</label>
      </div>
      <div className="form-floating mb-3">
        <input
          placeholder="Employee Id"
          type="text"
          id="employee_id"
          name="employee_id"
          value={formData.employee_id}
          onChange={handleChange}
          className="form-control"
        />
        <label htmlFor="employee_id">Employee ID:</label>
      </div>
      <button type="submit" className="btn btn-primary">Create Technician</button>
    </form>
    </div>
    </div>
    </div>
    </main>
  );
}


export default TechnicianForm;