import React, { useState, useEffect } from 'react';

function ModelForm () {
    const [ modelName, setModelName ] = useState('');
    const [ modelPhoto, setModelPhoto ] = useState('');
    const [ manufacturers, setManufacturers] = useState([]);
    const [ manufacturer, setManufactuer] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.name = modelName;
        data.picture_url = modelPhoto;
        data.manufacturer_id = manufacturer

        const modelUrl = 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(modelUrl, fetchConfig);
        if (response.ok) {
            const newModel = await response.json();

            setModelName('');
            setModelPhoto('');
            setManufactuer('');
        }

    }
    const fetchData = async () => {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }
    const handleModelNameChange =(event) => {
        const value = event.target.value;
        setModelName(value);
    }
    const handlePhotoChange =(event) => {
        const value = event.target.value;
        setModelPhoto(value);
    }
    const handleManufacturerChange =(event) => {
        const value = event.target.value;
        setManufactuer(value);
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h2>Create new Model</h2>
            <form onSubmit={handleSubmit} id="create-model-form">
              <div className="form-floating mb-3">
                <input onChange={handleModelNameChange} value={modelName} placeholder="Model" required type="text" name="model" id="model" className="form-control"/>
                <label htmlFor="model">Model</label>
              </div>
              <div className="form-floating mb-3">
              <input onChange={handlePhotoChange} value={modelPhoto} placeholder="picture_url"  required type="text" name="picture_url" id="picture_url" className="form-control"/>
              <label htmlFor="picture_url">Image URL</label>
            </div>
            <div className="mb-3">
              <select onChange={handleManufacturerChange} value={manufacturer} required name="manufacturer" id="manufacturer" className="form-select">
                <option value="">Choose a Manufacturer</option>
                {manufacturers.map(manufacturer => {
                    return (
                        <option value={manufacturer.id} key={manufacturer.id}>
                            {manufacturer.name}
                        </option>

                    );
                  })}
              </select>
            </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
        </div>
    )

}

export default ModelForm;
