import React, { useState, useEffect } from 'react';

function ManufacturerForm() {
    const [manufacturerName, setManufacturerName] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.name=manufacturerName;

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch (manufacturerUrl, fetchConfig);
        if (response.ok) {
            const newManufacturer = await response.json();

            setManufacturerName('');
        }

    }


    const handleChangeName = (event) => {
        const value = event.target.value;
        setManufacturerName(value);
    }
    return (
        <div className='row'>
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h2>Create Manufacturer</h2>
                    <form onSubmit={handleSubmit} id="create-manufacturer-form">
                        <div className="form-floating mb-3">
                            <input placeholder="Name" onChange={handleChangeName} value={manufacturerName}  required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="manufacturer">Name</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ManufacturerForm;
