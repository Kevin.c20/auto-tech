import React, { useEffect, useState } from "react";

function TechniciansList() {
  const [technicians, setTechnicians] = useState([]);

  const fetchTechnicians = async () => {
    const response = await fetch("http://localhost:8080/api/technicians/");

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  };

  useEffect(() => {
    fetchTechnicians();
  }, []);

  const handleDelete = async (id) => {
    const response = await fetch(`http://localhost:8080/api/technicians/${id}`, {
      method: "DELETE",
    });

    if (response.ok) {
      fetchTechnicians();
    }
  };

  return (
    <main className="container">
      <h1 className="h1-padded">Technicians</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {technicians.map((technician) => (
            <tr key={technician.id}>
              <td>{technician.employee_id}</td>
              <td>{technician.first_name}</td>
              <td>{technician.last_name}</td>
              <td>
                <button
                  className="btn btn-danger"
                  onClick={() => handleDelete(technician.id)}
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </main>
  );
}

export default TechniciansList;
