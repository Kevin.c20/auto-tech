from .models import Appointment, Technician, AutomobileVO
import json
from .encoders import TechnicianEncoder, AppointmentEncoder, AutomobileVOEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.db.models import Count

@require_http_methods(["GET", "POST"])
def api_technician_list(request):
    if request.method =="GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians":technicians},
            encoder = TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(
                first_name=content["first_name"],
                last_name=content["last_name"],
                employee_id=content["employee_id"],

            )
            return JsonResponse(
                {"technician": technician},
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the tech"}
            )
            response.status_code = 400
            return response
        
@require_http_methods(["GET", "DELETE"])
def api_detail_technicians(request, pk):
    if request.method == "GET":
        technician = Technician.objects.filter(id=pk)
        return JsonResponse(
            technician, encoder=TechnicianEncoder, safe=False
        )
    else:
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"delete": count > 0})



@require_http_methods(["GET", "POST"])
def appointments(request, vin=None):
    if request.method == "GET":
        if vin is not None:
            appointments = Appointment.objects.filter(vin=vin)
        else:
            appointments = Appointment.objects.all()

        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder
        )
    else:
        try:
            content = json.loads(request.body)

            appointment = Appointment.objects.create(
                vin=content["vin"],
                technician=Technician.objects.get(id=content["technician"]),
                status=Appointment.Statuses.CREATED,
                customer=content["customer"],
                date_time=content["date_time"],
                reason=content["reason"],
            )
            return JsonResponse(
                {"appointment": appointment},
                encoder=AppointmentEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not add the appointment"}
            )
            response.status_code = 400
            return response
@require_http_methods(["GET", "PUT", "DELETE"])
def appointment(request, pk, action=None):
    if request.method == "GET":
        appointments = Appointment.objects.get(id=pk)

        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False
        )

    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=pk)
            count, _ = appointment.delete()
            response = JsonResponse(
                {"deleted": count > 0}
            )
            return response
        except Appointment.DoesNotExist:
            response = JsonResponse(
                {"message": "Appointment does not exist"}
            )
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body) if request.body else {}

            if action.lower() == "finish":
                content["status"]=Appointment.Statuses.FINISHED

            if action.lower() == "cancel":
                content["status"]=Appointment.Statuses.CANCELED

            Appointment.objects.filter(id=pk).update(status=content["status"])
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                {"action": action,
                 "appointment": appointment},
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Exception as e:
            response = JsonResponse(
                {"message": "Could not update the appointment",
                 "error": e}
            )
            response.status_code = 400
            return response
@require_http_methods("GET")
def api_service_list(request):
    alltechs = Technician.objects.filter(appointments__isnull=False).values('id', 'employee_id', 'first_name', 'last_name').annotate(appt_count=Count('appointments'))
    listed = list(alltechs)
    try:
        return JsonResponse(
            listed,
            safe=False
        )
    except Technician.DoesNotExist:
        return JsonResponse(
        {"message": 'Invalid employee id'},
        status=400,
    )
