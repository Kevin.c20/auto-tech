from .models import Technician, AutomobileVO, Appointment
from common.json import ModelEncoder

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name", "last_name", "employee_id", "id"]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "import_href",
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "customer",
        "technician",
        "vin",
        "is_vip",
        "id",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }
