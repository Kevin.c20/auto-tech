from django.urls import path
from .views import api_detail_technicians, api_technician_list, appointment, appointments, api_service_list

urlpatterns = [
    path("technicians/", api_technician_list, name="api_list_technicians"),
    path("technicians/<int:pk>", api_detail_technicians, name="api_detail_technicians"),
    path("appointments/", appointments, name="appointments"),
    path("appointments/<int:pk>", appointment, name="appointment"),
    path("appointments/<int:pk>/<str:action>", appointment, name="appointment"),
    path("appointments/<int:pk>/<str:action>", appointment, name="appointment"),
    path("technitions/appointmentcount", api_service_list, name="api_service_by_technition")



]