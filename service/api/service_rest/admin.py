from django.contrib import admin

# Register your models here.
from .models import AutomobileVO, Technician, Appointment

admin.site.register(AutomobileVO)
admin.site.register(Technician)
admin.site.register(Appointment)