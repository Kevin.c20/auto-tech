# Generated by Django 4.0.3 on 2023-06-08 03:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0003_alter_sale_automobile_alter_sale_customer_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='customer',
            old_name='customer_name',
            new_name='first_name',
        ),
        migrations.RenameField(
            model_name='salesperson',
            old_name='salesperson_name',
            new_name='first_name',
        ),
        migrations.AddField(
            model_name='customer',
            name='last_name',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='salesperson',
            name='last_name',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
