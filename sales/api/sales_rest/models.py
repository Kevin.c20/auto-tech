from django.db import models
from django.urls import reverse

# Create your models here.
class AutomobileVO(models.Model):
    # import_href = models.CharField(max_length=200, null=True, unique=True)
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})


class Salesperson(models.Model):
    employee_id = models.CharField(max_length=20, unique=True)
    first_name = models.CharField(max_length=100, null=True)
    last_name = models.CharField(max_length=100, null=True)

    def get_api_url(self):
        return reverse("api_salesperson", kwargs={"pk": self.id})

    def __str__ (self):
        return self.first_name + " " + self.last_name

class Customer(models.Model):
    first_name = models.CharField(max_length=100, null=True)
    last_name = models.CharField(max_length=100, null=True)
    address = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=20)

    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.id})

    def __str__ (self):
        return self.first_name + " " + self.last_name

class Sale(models.Model):
    automobile = models.ForeignKey (
        AutomobileVO,
        related_name="sales",
        on_delete=models.CASCADE,
        null=True
    )
    salesperson = models.ForeignKey (
        Salesperson,
        related_name="sales",
        on_delete=models.CASCADE,
        null=True
    )
    customer = models.ForeignKey (
        Customer,
        related_name="sales",
        on_delete=models.CASCADE,
        null=True
    )
    price = models.PositiveIntegerField(null=True)

    def get_api_url(self):
        return reverse("api_sale", kwargs={"pk": self.id})
